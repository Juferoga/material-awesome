#!/bin/bash

echo "Empezando la instalacion de Awesome y sus dependencias"

function has_command(){
  command -v $1 > /dev/null
}

install_awesome() {
  if [ ! "$(which awesome 2> /dev/null)" ]; then
    prompt -i "\n 'awesome' necesita ser instalado"
    if has_command zypper; then
      sudo zypper in awesome
    elif has_command apt-get; then
      sudo apt-get install awesome rofi fonts-roboto compton i3lock-fancy xclip gnome-keyring policykit-1-gnome materia-gtk-theme lxappearance git
    elif has_command dnf; then
      sudo dnf install -y awesome
    elif has_command yum; then
      sudo yum install awesome
    elif has_command pacman; then
      sudo pacman -S --noconfirm awesome rofi compton i3lock xclip mate-polkit qt5-styles-gtk2 materia-theme lxappearance xbacklight flameshot git
    fi
  fi
}

install_awesome

echo "Instalando el tema light en español"

git clone https://gitlab.com/Juferoga/material-awesome.git ~/.config/awesome

echo "clonado realizado, se cerrara la sesion para comprobar la instalación"

sleep 30

sudo pkill -KILL -u $USER

